# GitHub Monitoring Application #

* GitHub monitoring(commits, updates) for users
* Add all updates from GitHub users to the Google Sheet

### Used technologies ###

* Python
* JSON
* Git
* GitHub API v3
* Google Sheets API v3
* PyGithub
* Google API Python Client

### How to execute ###

* Package 'keys' with keys should be added to the project
