import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint

from keys import keys
from modules.google_sheets_helper_class import CGoogleSheetHelper
from modules.user_class import CUser
from modules import app_config

class CGoogleSheetsAPI:
    __CREDENTIALS_FILE = keys.GOOGLE_SHEETS_API_KEY_MAIN
    __credentials = ServiceAccountCredentials.from_json_keyfile_name(__CREDENTIALS_FILE,
                                                                     ['https://www.googleapis.com/auth/spreadsheets',
                                                                      'https://www.googleapis.com/auth/drive'])
    __httpAuth = __credentials.authorize(httplib2.Http())
    __service = apiclient.discovery.build('sheets', 'v4', http=__httpAuth)

    __attr_google_sheets_helper = CGoogleSheetHelper()

    def __readRow(self, ranges):
        spreadsheet_id = keys.GOOGLE_SHEETS_API_SPREADHEED_ID
        value_render_option = 'FORMATTED_VALUE'
        date_time_render_option = 'SERIAL_NUMBER'
        include_grid_data = True
        request = self.__service.spreadsheets().values().get(spreadsheetId=spreadsheet_id, range=ranges,
                                                        valueRenderOption=value_render_option,
                                                        dateTimeRenderOption=date_time_render_option)
        response = request.execute()
        result = response.get('values')
        resultList = result[0]
        return resultList


    def updateCell(self, userID, newValue):
        spreadsheet_id = keys.GOOGLE_SHEETS_API_SPREADHEED_ID
        range_ = app_config.GOOGLE_SHEETS_API_NEW_COMMITS_COLUMN_INDEX_LETTER + str(userID)
        value_input_option = 'USER_ENTERED'
        value_range_body = {
            "majorDimension": "ROWS",
            "values": [
                [newValue],
            ],
        }
        request = self.__service.spreadsheets().values().update(spreadsheetId=spreadsheet_id, range=range_,
                                                           valueInputOption=value_input_option, body=value_range_body)
        response = request.execute()
        pprint(response)


    def colorCell(self, startColumnIndex, startRowIndex):
        spreadsheet_id = keys.GOOGLE_SHEETS_API_SPREADHEED_ID
        body = {
            "requests": [
                {
                    "repeatCell": {
                        "range": {
                            "sheetId": 0,
                            "startColumnIndex": startColumnIndex,
                            "startRowIndex": startRowIndex,
                            "endColumnIndex": startColumnIndex + 1,
                            "endRowIndex": startRowIndex + 1
                        },
                        "cell": {
                            "userEnteredFormat": {
                                "backgroundColor": {
                                    "red": 1.0,
                                    "green": 0,
                                    "blue": 0
                                }
                            }
                        },
                        "fields": "userEnteredFormat(backgroundColor)"
                    }
                }
            ]
        }
        response = self.__service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=body).execute()


    def getAllUsers(self):
        users = []
        while (True == self.__attr_google_sheets_helper.hasNextRow()):
            user = CUser()
            range = self.__attr_google_sheets_helper.getNextRowRange()
            row = self.__readRow(range)
            user.attr_id = self.__attr_google_sheets_helper.getCurrentRowNumber() - 1
            user.attr_name = row[app_config.NAME_INDEX]
            user.attr_github_user, user.attr_github_repo = self\
                .__attr_google_sheets_helper\
                .parseGitHubLinkToUserAndRepo(row[app_config.GITHUB_LINK_INDEX])
            users.append(user)
        return users


    def getCellIndexesForNewCommitsColorUpdate(self, userID):
        startColumnIndex = app_config.GOOGLE_SHEETS_API_NEW_COMMITS_COLUMN_INDEX_DIGIT
        startRowIndex = userID - 1
        return startColumnIndex, startRowIndex

        # cell = 'D1'
        # value = 'Email'
        # __updateCell(cell, value)


if __name__ == "__main__":
    inst = CGoogleSheetsAPI()
    list = inst.getAllUsers()
    for tmp in list:
        print(tmp.attr_id)
        print(tmp.attr_name)
        print(tmp.attr_github_user)
        print(tmp.attr_github_repo)
        print('--------')

