from github import Github
from keys import keys

class CGitHubAPI:
    __attr_gitHub = Github(client_id=keys.GITHUB_CLIENT_ID, client_secret=keys.GITHUB_SECRET_ID)

    def checkAndPrintLastUpdates(self, user, sinceDate):
        githubUser = self.__attr_gitHub.get_user(user.attr_github_user)
        print('User:', user.attr_name)

        githubRepo = githubUser.get_repo(user.attr_github_repo)
        print('Repo updated at:', githubRepo.updated_at)

        gitHubCommits = githubRepo.get_commits(since=sinceDate)
        print('Commits count:', len(list(gitHubCommits)), '  (since:', sinceDate, ')')

        for commit in gitHubCommits:
            if commit is not None:
                print(commit.commit.author.date)


    def fillCommits(self, user, sinceDate):
        githubUser = self.__attr_gitHub.get_user(user.attr_github_user)
        githubRepo = githubUser.get_repo(user.attr_github_repo)
        gitHubCommits = githubRepo.get_commits(since=sinceDate)
        user.attr_commitsInRepo = []

        for commit in gitHubCommits:
            if commit is not None:
                user.attr_commitsInRepo.append(commit.commit.author.date)