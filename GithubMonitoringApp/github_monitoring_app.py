from modules.google_sheet_api import CGoogleSheetsAPI
from modules.github_api import CGitHubAPI
from modules import app_config

class CGithubMonitoringApp:
    __attr_google_sheet_api = CGoogleSheetsAPI()
    __attr_github_api = CGitHubAPI()
    __attr_CHECK_FROM_DATE = app_config.DATE_FOR_COMMITS_CHECK

    def run(self):
        users = self.__attr_google_sheet_api.getAllUsers()
        for user in users:
            self.__attr_github_api.fillCommits(user, self.__attr_CHECK_FROM_DATE)

        for user in users:
            print('User:', user.attr_name)
            print('UserID:', user.attr_id)
            print('Commits count:', len(user.attr_commitsInRepo), '  (since:', self.__attr_CHECK_FROM_DATE, ')')

            if len(user.attr_commitsInRepo) > 0:
                startColumnIndex, startRowIndex = self.__attr_google_sheet_api.getCellIndexesForNewCommitsColorUpdate(user.attr_id)
                self.__attr_google_sheet_api.colorCell(startColumnIndex, startRowIndex)
            self.__attr_google_sheet_api.updateCell(user.attr_id, len(user.attr_commitsInRepo))
            print('---------------------')


if __name__ == "__main__":
    app = CGithubMonitoringApp()
    app.run()