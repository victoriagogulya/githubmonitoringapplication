from modules import app_config

class CGoogleSheetHelper:
    __attr_current_row = app_config.RANGE_ROW_START


    def getCurrentRowNumber(self):
        return self.__attr_current_row


    def hasNextRow(self):
        return self.__attr_current_row <= app_config.RANGE_ROW_END


    def getNextRowRange(self):
        result = app_config.RANGE_COLUMN_START + str(self.__attr_current_row) + \
                 ':' + app_config.RANGE_COLUMN_END + str(self.__attr_current_row)
        self.__attr_current_row += 1
        return result


     # Returns User and Repo
    def parseGitHubLinkToUserAndRepo(self, link):
        retValue = ['', '']
        strToReplace = 'https://github.com/'
        if link and (-1 != link.find(strToReplace)):
            result = link.replace(strToReplace, '')
            if result and (-1 != result.find('/')):
                retValue = result.split('/')
        return retValue